# UART used for console output.
# 1 by default.
CONSOLE_UART?=1

# UART used for HCI where used.
# 2 by default.
HCI_UART?=2

# HCI_UART_MAP used by default.
# Unset by default.
HCI_UART_MAP?=MAP_B

